//
//  ListAppointmentsTableViewController.h
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Patient.h"
#import "Appointment.h"
#import "AppointmentDetailViewController.h"

@interface ListAppointmentsTableViewController : UITableViewController

@property (nonatomic, strong) Patient *patient;

@end
