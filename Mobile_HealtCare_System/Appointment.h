//
//  Appointment.h
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/13/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <Parse/Parse.h>
#import "Doctor.h"
#import "Patient.h"

@interface Appointment : PFObject

@property (nonatomic, strong) Doctor *doctor;
@property (nonatomic, strong) Patient *patient;
@property (nonatomic, strong) NSString *diagnosis;
@property (nonatomic, strong) NSString *medicines;
@property (nonatomic, strong) NSString *disease;
@property (nonatomic) BOOL done;

+(NSString *)parseClassName;

@end
