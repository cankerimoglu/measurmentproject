//
//  PatientLoginViewController.h
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Patient.h"
#import "GetAppointmentViewController.h"

@interface PatientLoginViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tcnoTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UITextField *fullnameTextfield;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;

@end
