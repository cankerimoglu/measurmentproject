//
//  Doctor.h
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/13/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface Doctor : PFObject

@property (nonatomic, strong) PFUser *user;
@property (nonatomic, strong) NSString *fullName;

+(NSString *)parseClassName;

@end
