//
//  AppDelegate.h
//  Mobile_HealtCare_System
//
//  Created by CanCan on 13/12/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

