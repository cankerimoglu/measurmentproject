//
//  FirstViewController.m
//  Mobile_HealtCare_System
//
//  Created by CanCan on 13/12/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "GetAppointmentViewController.h"

@interface GetAppointmentViewController ()

@property (nonatomic, strong) PFUser *user;
@property (nonatomic, strong) NSArray *doctors;

@end

@implementation GetAppointmentViewController

- (void)viewDidLoad
{
    self.user = [PFUser currentUser];
    
    [super viewDidLoad];
    
    self.patientTcNo.text = self.user.username;
    self.patientName.text = self.patient.fullName;
    self.patientTcNo.enabled = NO;
    self.patientName.enabled = NO;
    
    self.doctorPickerView.dataSource = self;
    self.doctorPickerView.delegate = self;
    self.doctors = [NSArray array];
    
    PFQuery *query = [Doctor query];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.doctors = objects;
        [self.doctorPickerView reloadAllComponents];
    }];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.doctors.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Doctor *doctor = ((Doctor *)self.doctors[row]);
    if (doctor.fullName)
        return doctor.fullName;
    else
    {
        PFUser *user = (PFUser *)[doctor.user fetchIfNeeded];
        return user.username;
    }
}

- (IBAction)btnAppointment:(id)sender
{
    if ([PFUser currentUser])
    {
        self.user = [PFUser currentUser];
        [self saveAppointment];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Somethings Wrong" message:@"Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

-(void)saveAppointment
{
    Appointment *appointment = [Appointment object];
    appointment.patient = self.patient;
    appointment.disease = self.patientDisease.text;
    appointment.doctor = self.doctors[[self.doctorPickerView selectedRowInComponent:0]];
    [appointment save];
    
    [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Your appointment is approved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ListAppointmentsTableViewController *vc = segue.destinationViewController;
    vc.patient = self.patient;
}

@end
