//
//  AppointmentDetailViewController.h
//  Mobile_HealtCare_System
//
//  Created by Umut Bozkurt on 12/20/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Appointment.h"
#import "Patient.h"

@interface AppointmentDetailViewController : UIViewController<UIAlertViewDelegate>

@property (nonatomic, strong) Appointment *appointment;
@property (nonatomic) BOOL patientMode;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *diseaseLabel;
@property (weak, nonatomic) IBOutlet UITextField *diagnosisTextfield;
@property (weak, nonatomic) IBOutlet UITextField *medicineTextfield;


@end
