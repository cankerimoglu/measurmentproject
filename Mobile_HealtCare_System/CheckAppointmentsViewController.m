//
//  SecondViewController.m
//  Mobile_HealtCare_System
//
//  Created by CanCan on 13/12/14.
//  Copyright (c) 2014 Can Kerimoglu. All rights reserved.
//

#import "CheckAppointmentsViewController.h"

@interface CheckAppointmentsViewController ()

@property (nonatomic, strong) PFUser *user;
@property (nonatomic, strong) NSArray *appointments;

@end

@implementation CheckAppointmentsViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [PFUser currentUser];
    self.appointments = [NSArray array];
    
    UIRefreshControl *pullToRefresh = [[UIRefreshControl alloc] init];
    [pullToRefresh addTarget:self action:@selector(reloadAll:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:pullToRefresh];
    
    self.title = [NSString stringWithFormat:@"Dr. %@", self.doctor.fullName];
    
    [self fetchData];
}

-(void)fetchData
{
    PFQuery *query = [Appointment query];
    [query whereKey:@"doctor" equalTo:self.doctor];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.appointments = objects;
        [self.tableView reloadData];
    }];
}

-(void)reloadAll:(UIRefreshControl *)control
{
    [control endRefreshing];
    
    [self fetchData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Appointment *appointment = self.appointments[indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"patientCell" forIndexPath:indexPath];
    NSString *patientName = ((Patient *)[appointment.patient fetchIfNeeded]).fullName;
    cell.textLabel.text = patientName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ on %@", appointment.disease, appointment.createdAt];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.appointments.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"detail" sender:self.appointments[indexPath.row]];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AppointmentDetailViewController *vc = segue.destinationViewController;
    vc.appointment = sender;
}

@end
